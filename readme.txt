"Pearson Exotronics" by tomatopaste. First started development in March 2020.
Art, sounds and programming are done by me, additional help is documented below. This isn't a locked vault, I'm
happy to share programming assets if asked and it's a reasonable request. The drone framework is somewhat modular so hit
me up if interested ;). Please enjoy!

Additional credit:
-Credit to MesoTroniK and Histidine for the original random mission generation script from Tiandong.
-Credit to various folks that have given feedback and reported issues, thank you :)

My other mods:
-Lights Out - forum link: https://fractalsoftworks.com/forum/index.php?topic=18416.0
-Total Conversion: Salvage and Solder - not released, still in development
-Kipling Radiative (defunct and bad) - forum link: ded